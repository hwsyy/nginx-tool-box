var express = require('express');
var bodyParser = require("body-parser");
var rf = require("fs");
var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('asserts'));
app.get("/tasks",function(_req,_res){
	_res.send('{"tasklist":[{"name":"aaa","time":111},{"name":"bbb","time":222}]}');
});

app.get("/configfile",function(_req,_res){
	//获取nginx配置文件
  var data = rf.readFileSync("conf.d/default.conf","utf-8");
	_res.send(data);
});
app.post("/configfile",function(_req,_res){

	rf.writeFile("conf.d/default.conf",_req.body.content,function(err){
		console.log(_req.body.content);
		_res.send("ok");
	});
});
app.get("/rebootNginx",function(_req,_res){
	//执行跟新脚本
	var calfile = require("child_process");
	console.log("file");
	_res.send("ok")
	calfile.execFile("conf.d/update.sh",null,null,function(err,stdout,stderr){
		console.log(err+"/n"+stdout);
	});

});

var server = app.listen(3000, function () {
	var host = server.address().address;
	var port = server.address().port;

	console.log('Example app listening at http://%s:%s', host, port);
});
